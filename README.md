### Parabola GNU/Linux-libre Cheat Cube

Parabola Cheat Cube was inspired by and based on [Arch Cheat Cube](https://github.com/Thomas-Do/Arch-Cheat-Cube) by Thomas-Do and lunes.

Notable changes/improvements:

- It has been made modular to make it easier to modify (replace faces, fit to other distros, etc.)
- It has been adapted to Parabola the [deblobbed](https://en.wikipedia.org/wiki/Linux-libre) and fully free (as in freedom) GNU/Linux-libre distribution based Arch Linux.
- 3 new faces have been introduced so far:
    - a [Parabola logo with mascots](https://gitlab.com/GergelySzekely/Parabola-Cheat-Cube/blob/master/face-parabola-mascot-logo.svg) using [Parabola logo, GNU and Bola.svg](https://commons.wikimedia.org/wiki/File:Parabola_logo,_GNU_and_Bola.svg) by Crazytoon and coadde
    - a simple [Parabola logo](https://gitlab.com/GergelySzekely/Parabola-Cheat-Cube/blob/master/face-parabola-logo.svg) 
    - a face for [file searching](https://gitlab.com/GergelySzekely/Parabola-Cheat-Cube/blob/master/face-searching.svg) with ```find``` command 


All comments, suggestions and improvements are welcomed.
